#!/bin/bash
#Update System
sudo pacman -Syu

# Install Paru
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si

APPS="
arc-gtk-theme
alacritty
dosfstools
discord
dmenu
dunst
efibootmgr
fish
git
grub
grub-customizer
htop
i3-wm
i3status-rust
keepassxc
librewolf
man-db
mtools
neovim
notepadqq
openssh
os-prober
pavucontrol
pcmanfm
pulseaudio-alsa
qbittorrent
starship
steam
sudo
virtualbox
vlc
yubioath-desktop
"

AUR="
j4-dmenu-desktop
nerd-fonts-source-code-pro
noisetorch
spotify
yadm
"

#Install Packages
paru -S --needed $APPS $AUR
