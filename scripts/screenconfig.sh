#!/bin/bash
xrandr --auto --output DP-0 --mode 2560x1440 --rate 144 --primary
xrandr --auto --output HDMI-0 --mode 1920x1080 --left-of DP-0 --rate 60
xrandr --auto --output DP-4.8 --mode 2560x1440 --right-of DP-0 --rate 60
